from texts_handler import get_dictionary, get_entry_sentences, save_sentences
from longest_common_subsequence import lcs_repair_all_sentences, get_lcs_length
from edit_distance import ed_repair_all_sentences, get_ed_cost
from strong_repair import strong_repair_all_sentences
from testing import compare_sentences_in_file

slovnik = get_dictionary('slovnik.txt')
chvstup = get_entry_sentences('chvstup.txt')

print('\n-------------------------------------------------- LCS repair --------------------------------------------------\n')

vystup = lcs_repair_all_sentences(chvstup, slovnik)
save_sentences('lsc_vystup.txt', vystup)
print('ORIGINAL: ', chvstup)
print('LCS REPAIRED: ', vystup)
result = compare_sentences_in_file('pvstup.txt', 'lsc_vystup.txt')
print('RESULT: ', result, '% success')

print('\n-------------------------------------------------- LCS 2 words --------------------------------------------------\n')

X = "STUFEI"
Y = "STFCII"
print('1st word: ', X)
print('2nd word: ', Y)
print("Length of LCS is ", get_lcs_length(X, Y))

print('\n-------------------------------------------------- ED repair --------------------------------------------------\n')

vystup = ed_repair_all_sentences(chvstup, slovnik)
save_sentences('ed_vystup.txt', vystup)
print('ORIGINAL: ', chvstup)
print('ED REPAIRED: ', vystup)
result = compare_sentences_in_file('pvstup.txt', 'ed_vystup.txt')
print('RESULT: ', result, '% success')

print('\n-------------------------------------------------- ED 2 words --------------------------------------------------\n')

X = "STUFEI"
Y = "STFCII"
print('1st word: ', X)
print('2nd word: ', Y)
print("Cost of ED ", get_ed_cost(X, Y))


print('\n-------------------------------------------------- STRONG repair --------------------------------------------------\n')

vystup = strong_repair_all_sentences(chvstup, slovnik)
save_sentences('strong_vystup.txt', vystup)
print('ORIGINAL: ', chvstup)
print('STRONG REPAIRED: ', vystup)
result = compare_sentences_in_file('pvstup.txt', 'strong_vystup.txt')
print('RESULT: ', result, '% success')

