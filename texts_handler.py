from pathlib import Path
root = str(Path(__file__).absolute().parent)


def get_dictionary(file_name):
    path = root + '/texts/' + file_name
    with open(path, "r") as file:
        return file.read().splitlines()


def get_entry_sentences(file_name):
    sentences = get_dictionary(file_name)
    sentences_array = []
    for sentence in sentences:
        sentences_array.append(sentence.split(' '))
    return sentences_array


def save_sentences(file_name, sentences):
    path = root + '/texts/' + file_name
    with open(path, 'w') as f:
        for sentence in sentences:
            sentence_text = ""
            for word in sentence:
                sentence_text += word + " "
            sentence_text = sentence_text[:-1] + "\n"
            f.write(sentence_text)
    f.close()



