
def ed_repair_all_sentences(sentences, dictionary):
    all_repaired_sentences = []
    for sentence in sentences:
        repaired_sentence = ed_repair_sentence(sentence, dictionary)
        all_repaired_sentences.append(repaired_sentence)
    return all_repaired_sentences


def ed_repair_sentence(sentence, dictionary):
    repaired_sentence = []
    for word in sentence:
        repaired_word = ed_repair_word(word, dictionary)
        print('Original word: ', word, ' |#| Repaired word: ', repaired_word)
        repaired_sentence.append(repaired_word)
    return repaired_sentence


def ed_repair_word(word, dictionary):
    best_cost = -1
    best_word = None
    for dic_word in dictionary:
        if word.lower() == dic_word.lower():
            return word
        cost = get_ed_cost(word.lower(), dic_word.lower())
        if best_cost == -1 or cost < best_cost:
            best_cost = cost
            best_word = dic_word
    if best_word is None:
        return word
    else:
        return best_word


def get_ed_cost(word, dic_word):
    word_length = len(word)
    dic_word_length = len(dic_word)

    costs = [[0 for _ in range(dic_word_length + 1)] for _ in range(word_length + 1)]

    # iterate through all substrings from 0 to i
    for i in range(word_length + 1):
        for j in range(dic_word_length + 1):
            if i == 0:
                # substring is empty so we must remove everything from other substring
                costs[i][j] = j
            elif j == 0:
                # substring is empty so we must remove everything from other substring
                costs[i][j] = i
            elif word[i - 1] == dic_word[j - 1]:
                # if last chars are same, take diagonally last cost
                costs[i][j] = costs[i - 1][j - 1]
            else:
                # if last chars are NOT same, find minimum from costs of INSERT(top), REMOVE(left), REPLACE(diagonally)
                costs[i][j] = 1 + min(costs[i][j - 1], costs[i - 1][j], costs[i - 1][j - 1])

    return costs[word_length][dic_word_length]
