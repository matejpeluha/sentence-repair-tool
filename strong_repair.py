from edit_distance import get_ed_cost
from longest_common_subsequence import get_lcs_length


def strong_repair_all_sentences(sentences, dictionary):
    all_repaired_sentences = []
    for sentence in sentences:
        repaired_sentence = strong_repair_sentence(sentence, dictionary)
        all_repaired_sentences.append(repaired_sentence)
    return all_repaired_sentences


def strong_repair_sentence(sentence, dictionary):
    repaired_sentence = []
    for word in sentence:
        repaired_word = strong_repair_word(word, dictionary)
        print('Original word: ', word, ' |#| Repaired word: ', repaired_word)
        repaired_sentence.append(repaired_word)
    return repaired_sentence


def strong_repair_word(word, dictionary):
    ideal = 1 + (len(word) - 1)
    best_ideal_diff = -1
    best_cost = -1
    best_lcs_length = 0
    best_word = None
    for dic_word in dictionary:
        if word.lower() == dic_word.lower():
            return word
        cost = get_ed_cost(word.lower(), dic_word.lower())
        lcs_length = get_lcs_length(word.lower(), dic_word.lower())
        if best_cost == -1 and best_ideal_diff == -1:
            best_cost = cost
            best_lcs_length = lcs_length
            best_ideal_diff = abs(ideal - (cost + lcs_length))
            best_word = dic_word
        elif cost == best_cost and lcs_length == best_lcs_length:
            continue
        elif cost <= best_cost and lcs_length >= best_lcs_length:
            best_cost = cost
            best_lcs_length = lcs_length
            best_ideal_diff = abs(ideal - (cost + lcs_length))
            best_word = dic_word
        else:
            new_ideal_diff = abs(ideal - (cost + lcs_length))
            if new_ideal_diff < best_ideal_diff:
                best_cost = cost
                best_lcs_length = lcs_length
                best_ideal_diff = new_ideal_diff
                best_word = dic_word


    if best_word is None:
        return word
    else:
        return best_word