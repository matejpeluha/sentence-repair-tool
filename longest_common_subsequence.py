

def lcs_repair_all_sentences(sentences, dictionary):
    all_repaired_sentences = []
    for sentence in sentences:
        repaired_sentence = lcs_repair_sentence(sentence, dictionary)
        all_repaired_sentences.append(repaired_sentence)
    return all_repaired_sentences


def lcs_repair_sentence(sentence, dictionary):
    repaired_sentence = []
    for word in sentence:
        repaired_word = lcs_repair_word(word, dictionary)
        print('Original word: ', word, ' |#| Repaired word: ', repaired_word)
        repaired_sentence.append(repaired_word)
    return repaired_sentence


def lcs_repair_word(word, dictionary):
    best_lcs_length = 0
    best_word = None
    for dic_word in dictionary:
        if word.lower() == dic_word.lower():
            return word
        lcs_length = get_lcs_length(word.lower(), dic_word.lower())
        if lcs_length > best_lcs_length:
            best_lcs_length = lcs_length
            best_word = dic_word
    if best_word is None:
        return word
    else:
        return best_word


def get_lcs_length(word, dic_word):
    word_length = len(word)
    dic_word_length = len(dic_word)

    # declaring the array for storing the lsc lengths
    lsc_lengths = [[0] * (dic_word_length + 1) for _ in range(word_length + 1)]

    # iterate through words to try all subsequences of both words
    for row_index in range(1, word_length + 1):
        for column_index in range(1, dic_word_length + 1):
            # subsequence of bot words from 0 to i
            if word[row_index - 1] == dic_word[column_index - 1]:
                # if both last characters of sequences are same
                lsc_lengths[row_index][column_index] = lsc_lengths[row_index - 1][column_index - 1] + 1
            else:
                # if both last characters of sequences are NOT same
                lsc_lengths[row_index][column_index] = max(lsc_lengths[row_index - 1][column_index], lsc_lengths[row_index][column_index - 1])

    return lsc_lengths[word_length][dic_word_length]