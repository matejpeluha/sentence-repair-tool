from texts_handler import get_entry_sentences


def compare_sentences_in_file(right_file_name, repaired_file_name):
    right_sentences = get_entry_sentences(right_file_name)
    repaired_sentences = get_entry_sentences(repaired_file_name)

    if len(right_sentences) != len(repaired_sentences):
        return -1

    same_words_counter = 0
    total_words_counter = 0

    for i in range(len(right_sentences)):
        right_sentence = right_sentences[i]
        repaired_sentence = repaired_sentences[i]
        for j in range(len(right_sentence)):
            right_word = right_sentence[j]
            repaired_word = repaired_sentence[j]
            if right_word == repaired_word:
                same_words_counter += 1
            total_words_counter += 1

    return same_words_counter / total_words_counter
